[![Build Status](https://travis-ci.org/belchior/calc.svg?branch=react-redux)](https://travis-ci.org/belchior/calc)
[![Coverage Status](https://coveralls.io/repos/github/belchior/calc/badge.svg)](https://coveralls.io/github/belchior/calc)
[![License MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)


# Calculadora Aritmética
> Elegantes, modernas e divertidas.

[Demo](https://belchior.github.io/calc)

## Suporte
Chrome, Firefox, Safari e Edge

## Licença
MIT

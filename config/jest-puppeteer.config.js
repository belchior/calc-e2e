const config = require('./puppeteer.config');

module.exports = {
  launch: { ...config.launchOptions },
}

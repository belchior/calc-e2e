
module.exports = {
  appUrl: 'https://belchior.github.io/calc',
  launchOptions: {
    args: ['--start-maximized'],
    dumpio: false,
    devtools: false,
    headless: true,
    slowMo: 0,
  }
};
